import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import Dashboard from "./pages/dashboard";
import Login from "./pages/login";
import Register from "./pages/register";
import MyDashboard from "./pages/myDashboard/index"

const App = () => {
  return (
    <Router>
      <Route exact path={"/"} component={Login} />
      <Route path={"/user"} component={Dashboard} />
      <Route exact path={"/register"} component={Register} />
      <Route path={"/dashboard"} component={MyDashboard} />
    </Router>
  )
}

export default App;
