import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import moment from 'moment';
const kelvinToCelsius = require('kelvin-to-celsius');


const WeatherWidget = ({ setting1 }) => {
    // console.log('setting1111 :', setting1);
    const classes = useStyles();
    const [data, setData] = useState([]);
    const searchCity = setting1;
    // const searchCity = 'Lille';
    // const date = Date.now();

    useEffect(() => {
        fetch(`https://api.openweathermap.org/data/2.5/weather?q=${searchCity}&APPID=64a9800e78df4078113598c3a1392e76`)
            .then(response => response.json())
            .then(response => {
                setData(response);
            })
            .catch(err => { console.log('error', err) });
    }, [searchCity]);

    const dayDT = moment.unix(data.dt);
    const day = moment.unix(dayDT);

    return (
        <>
            {data &&
                <Card className={classes.root}>
                    <CardHeader
                        title={searchCity}
                        subheader={day.format('dddd h:mm A')}
                    />
                    {data.sys &&
                        <Typography className={classes.pos} color="textSecondary">
                            {data.sys.country}
                        </Typography>
                    }
                    {data.weather &&
                        <CardMedia
                            className={classes.media}
                            image={`http://openweathermap.org/img/w/${data.weather[0].icon}.png`}
                            title={data.weather[0].description}
                        // alt={data.weather[0].description}
                        />
                    }
                    <CardContent>
                        {data.weather &&
                            <Typography variant="body2" color="textSecondary" component="p" style={{marginTop: "-20px"}}>
                                {data.weather[0].main}
                            </Typography>
                        }

                        {data.main &&
                            <Typography variant="body2" color="textSecondary" component="p" style={{marginBottom: "-20px"}}>
                                {kelvinToCelsius(data.main.temp_min)} &nbsp;<b>|</b>&nbsp; {kelvinToCelsius(data && data.main.temp_max)}
                            </Typography>
                        }
                    </CardContent>
                </Card>
            }
        </>
    )
}

const useStyles = makeStyles(theme => ({
    root: {
        maxWidth: 345,
        textAlign: "center"
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
        maxWidth: '100%'
    },
}));

export default WeatherWidget;