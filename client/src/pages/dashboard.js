import queryString from "query-string";
import React, { useState, useEffect } from 'react';
import Header from '../components/header';
import Services from './ServicesPage';

const Dashboard = ({ history: { location } }) => {

    const [users, setUsers] = useState([]);
    const { search } = location;
    const user = queryString.parse(search).id;

    useEffect(() => {
        fetch("http://localhost:8080/users/" + user)
            .then(res => res.json())
            .then(res => setUsers(res));
    }, []);

    return (
        <>
            <Header data={user} /><br />
            {/* <div>
                <Button>  logout </Button>
            </div> */}
            <Services data={user} ></Services>
        </>
    )
}

export default Dashboard;