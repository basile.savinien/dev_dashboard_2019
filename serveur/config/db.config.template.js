module.exports = {
    HOST: "",
    USER: "",
    PASSWORD: "",
    DB: "",
    facebook_api_key: "",
    facebook_api_secret: "",
    callback_url: "",
    use_database: true,
};